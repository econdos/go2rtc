import {VideoRTC} from "./video-rtc.js";

class VideoStream extends VideoRTC {
    state = '';

    set divFeedback(content) {
        this.querySelector('.info').innerHTML = content;
    }

    /**
     * Custom GUI
     */
    oninit() {
        console.debug("stream.oninit");
        super.oninit();

        this.innerHTML = `
            <style>
                video-stream {
                    position: relative;
                }

                .info {
                    position: absolute;
                    inset: 0;
                }

                .error-feedback {
                    height: 100%;

                    display: flex;
                    align-items: center;
                    justify-content: center;
                }

                .error-feedback > p {
                    margin: 0;
                    opacity: 0.75;

                    font-size: 0.75rem;
                    font-weight: bold;
                    color: white;
                }

                .error-details {
                    display: none;
                }
            </style>
            <div class="info"></div>
        `;

        const info = this.querySelector(".info")
        this.insertBefore(this.video, info);
    }

    onconnect() {
        console.debug("stream.onconnect");
        const result = super.onconnect();
        if (result) this.showLoadingFeedback();
        return result;
    }

    ondisconnect() {
        console.debug("stream.ondisconnect");
        super.ondisconnect();
    }

    onopen() {
        console.debug("stream.onopen");
        const result = super.onopen();

        this.onmessage["stream"] = msg => {
            console.debug("stream.onmessge", msg);

            switch (msg.type) {
                case 'error':
                    this.isWebRtcErrored = msg.value.startsWith('webrtc');

                    const cameraId = this.getAttribute('data-cameraId');

                    if (this.isWebRtcErrored && this.isMseErrored) {
                        ['error', 'stalled'].forEach(event => {
                            this.video.addEventListener(event, () => {
                                this.showErrorFeedback('Não foi possível carregar a imagem da câmera em MP4');
                                console.log(`\n\n[eCamera]\nErro ao carregar imagem da câmera "${cameraId}"\nERRO: Não foi possível carregar a imagem da câmera em MP4\n\n`);
                            });
                        });

                        ['play', 'playing'].forEach(event => {
                            this.setMode('MP4');
                            this.video.addEventListener(event, () => this.clearFeedback());
                        });

                        ['loadstart', 'waiting'].forEach(event => {
                            this.video.addEventListener(event, () => this.showLoadingFeedback());
                        });
                        
                        this.video.src = `http://localhost:1984/api/stream.mp4?src=${cameraId}`;
                        this.play();
                    } else {
                        this.showErrorFeedback(msg.value);
                        console.log(`\n\n[eCamera]\nErro ao carregar imagem da câmera "${cameraId}"\nERRO: ${msg.value}\n\n`);
                    }
                    break;
                case 'mse':
                case 'hls':
                case 'mp4':
                case 'mjpeg':
                    this.state = 'success';
                    this.setMode(msg.type);
                    this.clearFeedback();
                    break;
            }
        }

        return result;
    }

    onclose() {
        console.debug("stream.onclose");
        return super.onclose();
    }

    onpcvideo(ev) {
        console.debug("stream.onpcvideo");
        super.onpcvideo(ev);
    }

    setMode(mode) {
        this.video.setAttribute('data-mode', mode.toUpperCase());
    }

    clearFeedback() {
        this.video.setAttribute('data-error', '');
        this.divFeedback = '';
    }

    showErrorFeedback(message) {
        if (this.state !== 'loading') {
            return;
        }

        this.state = 'error';

        this.video.setAttribute('data-error', message);

        this.divFeedback = `
            <div class="error-feedback">
                <p>⚠️ Imagem indisponível</p>
            </div>
        `;
    }

    showLoadingFeedback() {
        this.state = 'loading';

        this.divFeedback = `
            <div class="error-feedback">
                <p>Carregando...</p>
            </div>
        `;
    }
}

customElements.define("video-stream", VideoStream);
